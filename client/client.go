package main

import (
	"netutils"
	"crypto_cipher"
	"flag"
	"time"
	"fmt"
	"net"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket"
)

type net_info struct {
	sMac []byte
	dMac []byte
	src  []byte
	dst  []byte
}

func make_request(h *pcap.Handle, p *gopacket.PacketSource, n *net_info, data []byte)([]byte){
	var ipLayer layers.IPv4
	var ethLayer layers.Ethernet
	var payload gopacket.Payload
	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet, &ethLayer, &ipLayer, &payload)
	decoded := make([]gopacket.LayerType, 0, 4)
	for {
		h.WritePacketData(netutils.CraftProtocolPacket(n.sMac, n.dMac, n.src, n.dst,data))
		fmt.Println("SEnded", data)
		time.Sleep(100 * time.Millisecond)
		tries := 0
		var err error
		for {
			packet, err := p.NextPacket()
			if (err != nil) && (tries != 200) {
				tries++
		//		fmt.Println(err)
				continue
			}
			if tries != 200 {
				err = parser.DecodeLayers(packet.Data(), &decoded)
			}
			break
		}
		if tries == 200 {
			continue
		}
		if (err != nil){
			if(err.Error() != "No decoder for layer type Unknown") {
				fmt.Println(err)
				continue
			}
		}
		return ipLayer.LayerPayload()[1:]
	}
}

func get_my_byte(h *pcap.Handle,p *gopacket.PacketSource, n *net_info, prefix, suffix  []byte, err_s string) ([]byte){
	pos := len(prefix)
	msg := append(prefix,0)
	msg = append(msg, suffix...)
	var brt byte
	brt = 0
	ret := make([]byte, 0)
	for {
		msg[pos] = brt
		answer_err := string(make_request(h, p, n, msg))
		if answer_err != err_s {
			fmt.Println("Found gamma byte ", brt, " ", answer_err)
			ret = append(ret, brt)
		}
		brt += 1
		if brt == 0 {
			break
		}
	}
	return ret
}

func main(){
	ipPtr := flag.String("ip", "127.0.0.1", "The ip to connect")
	sMacString := flag.String("mac", "", "Source mac address")
	ifname := flag.String("ifname", "eth0", "Interface name")
	flag.Parse()
	if *sMacString == "" {
		fmt.Println("You should specify source MAC address")
		return
	}
	parsedIP := net.ParseIP(*ipPtr)
	server_ip := []byte(*ipPtr)
	fmt.Println(parsedIP[12:16])
	sMac, _ := net.ParseMAC(*sMacString)
	dMac, src := netutils.GetGatewayMACAndSelfIP(*ifname, sMac, parsedIP[12:16])
	fmt.Println("Got gateway  mac {}",dMac)
	hSocket, err := pcap.OpenLive(*ifname, 1600, true, 100*time.Millisecond)
	if err != nil {
		panic(err)
	}
	packetSource := gopacket.NewPacketSource(hSocket, hSocket.LinkType())
	defer hSocket.Close()
	err = hSocket.SetBPFInstructionFilter([]pcap.BPFInstruction{
		{ 0x28, 0, 0, 0x0000000c },
		{ 0x15, 0, 3, 0x00000800 },
		{ 0x30, 0, 0, 0x00000017 },
		{ 0x15, 0, 1, 0x00000063 },
		{ 0x6, 0, 0, 0x0000ffff },
		{ 0x6, 0, 0, 0x00000000 },
	})
	if err != nil {
		panic(err)
	}
	needed_bytes := 16 + 5 
	var n net_info
	n.sMac = sMac
	n.dMac = dMac
	n.src = src
	n.dst = parsedIP
	first := get_my_byte(hSocket, packetSource, &n, []byte{1},[]byte{}, "Invalid length")
	if len(first) != 1 {
		fmt.Println("Error")
		return
	}
	fmt.Println(first[0])
	gamma := make([]byte, 0)
	gamma = append(gamma, first[0])
	second := get_my_byte(hSocket, packetSource, &n, []byte{0, first[0]^1},[]byte{}, "Invalid command")
	fmt.Println(second)
	for i:=0; i< len(second);i++{
		data := string(make_request(hSocket, packetSource, &n, []byte{0, gamma[0]^1, second[i]}))
		if data == "Got 0 params - expected 1" {
			gamma = append(gamma, second[i]^2)
		}

	}
	
	data := make_request(hSocket, packetSource, &n, []byte{123, gamma[0]^1, gamma[1]^5})
	fmt.Println(data)
	fmt.Println(gamma)
	ln := len(data)
	bts := make([]byte, len(data))
	bts[0] = byte(ln-1) ^ data[0]
	fmt.Println(bts)
	bts[1] = data[1] ^ 7
	fmt.Println(bts)
	bts[2] = byte(ln-3) ^ data[2]
	fmt.Println(bts)
	bts[3] = data[3] ^ 1
	fmt.Println(bts)
	bts[4] = byte(ln-5) ^ data[4]
	fmt.Println(bts)
	fmt.Println(len(server_ip))
	var curr byte
	fmt.Println(len(data))
	for i:=5;i<len(data);i++{
		fmt.Println(i)
		curr = data[i] ^ server_ip[i-5]
		fmt.Println(i-5,":",bts)
		bts[i] = curr
	}
	fmt.Println(bts)
	for i:=len(data);i<needed_bytes;i++{
		msg := make([]byte, i+1)
		msg[0] = bts[0]
		msg[1] = bts[0]^byte(i)
		msg[2] = bts[1]
		msg[3] = bts[2]^byte(i-2)
		msg[4] = bts[3]^byte(i-3)
		for j:=5;j<=i;j++{
			msg[j] = bts[j-1]
		}
		new_byte := get_my_byte(hSocket, packetSource, &n, msg,[]byte{}, "Error while parsing command params")
		if len(new_byte) == 0 {
			fmt.Println("packet loss but delivered after some time.Need rerun")
			return
		}
		bts = append(bts, new_byte[0])
		fmt.Println(bts)
	}
	fmt.Println("heyo")
	fmt.Println(bts)
	msg := []byte{0}
	msg = append(msg, bts...)
	fmt.Println(make_request(hSocket, packetSource, &n, msg))
	fmt.Println("heyo")
	command := []byte{1, bts[0]^12, bts[1]^2, bts[2]^10, bts[3]^1, bts[4]^8}
	fname := "flag.txt"
	for i:=0;i < len(fname); i++ {
		command = append(command, bts[5+i]^fname[i])
	}
	data = make_request(hSocket, packetSource, &n, command)
	s := make([]byte, len(bts))
	fmt.Println(len(bts)," ", len(data))
	for i:=0;i<len(bts);i++{
		s[i] = data[i]^bts[i]
	}
	fmt.Println([]byte(s))
	fmt.Println("Key is", string(s[5:]))
	fmt.Println("File contents", string(crypto_cipher.Encrypt(data, s[5:])))
	/*
	third := get_my_byte(hSocket, packetSource, &n, []byte{1, first[0]^3, second[0]},[]byte{0},"Invalid length")
	fmt.Println(third)
	fourth := get_my_byte(hSocket, packetSource, &n, []byte{0, first[0]^3, second[2], third[0]},[]byte{},"Error while parsing command params")
	fmt.Println(fourth)
	fifth := get_my_byte(hSocket, packetSource, &n, []byte{0, first[0]^4, second[2], third[0]^1^2, fourth[0]^1},[]byte{},"Error while parsing command params")
	fmt.Println(fifth)
	sixth := get_my_byte(hSocket, packetSource, &n, []byte{0, first[0]^5, second[2], third[0]^1^3, fourth[0]^2, fifth[0]},[]byte{},"Error while parsing command params")
	fmt.Println(sixth)
	//var ipLayer layers.IPv4
	//var ethLayer layers.Ethernet
	//var payload gopacket.Payload
	//parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet, &ethLayer, &ipLayer, &payload)
	//decoded := make([]gopacket.LayerType, 0, 4)
	hSocket.WritePacketData(netutils.CraftProtocolPacket(sMac, dMac, src, parsedIP,[]byte{0, first[0]^5, second[2], third[0]^1^3 ,fourth[0]^1, fifth[0]^1, 97^sixth[0] }))
	*/
}
