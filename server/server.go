package main
	
import (
	"fmt"
	"net"
	"flag"
	"os"
	"netutils"
	"protocol"
	"crypto_cipher"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket"
)


var (
	hMainSocket *pcap.Handle
)

type packet_data struct{
	dIp net.IP
	id byte
	data []byte
}


func err_chksum(s string)(uint){
	var sum uint
	for i:=0; i < len(s);i++{
		sum += uint(s[i])
	}
	return sum
}

func main() {
	iface := flag.String("iface", "eth0", "Interface name")
	mac:= flag.String("mac", "", "Self MAC address")
	flag.Parse()
	if *mac == "" {
		fmt.Println("No MAC address specified")
		return
	}	
	sMac,_ := net.ParseMAC(*mac)
	hMainSocket, err := pcap.OpenLive("eth0", 1600, true, pcap.BlockForever)
	if err != nil {
		panic(err)	
	}
	//err = hMainSocket.SetBPFFilter("ip proto gre")
	//This will be much more fun to reverse
	err = hMainSocket.SetBPFInstructionFilter([]pcap.BPFInstruction{
		{ 0x28, 0, 0, 0x0000000c },
		{ 0x15, 0, 3, 0x00000800 },
		{ 0x30, 0, 0, 0x00000017 },
		{ 0x15, 0, 1, 0x00000063 },
		{ 0x6, 0, 0, 0x0000ffff },
		{ 0x6, 0, 0, 0x00000000 },
	})

	fmt.Println(err_chksum("No decoder for layer type Unknown"))
	if err != nil {
		panic(err)
	}
	fkey, err := os.Open("flag.txt")
	if err != nil {
		panic(err)
	}
	key := make([]byte, 16)
	_, err = fkey.Read(key)
	if err != nil {
		panic(err)
	}
	dMac,selfIp := netutils.GetGatewayMACAndSelfIP(*iface, sMac,  net.ParseIP("8.8.8.8")[12:16])
	var ipLayer layers.IPv4
	var ethLayer layers.Ethernet
	var payload gopacket.Payload
	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet, &ethLayer, &ipLayer, &payload)
	packet_chan := make(chan packet_data, 400)
	decoded := make([]gopacket.LayerType, 0, 4)
	parsed := make(chan packet_data, 400)
	processed := make(chan packet_data, 400)
	go func() {
		for {
			packet := <-packet_chan
			msg := crypto_cipher.Encrypt(packet.data, key)
			var answer packet_data
			answer.dIp = packet.dIp
			answer.id = packet.id
			answer.data = msg
			//fmt.Println("Got message: ",answer.id, " ",  msg)
			parsed <- answer
		}
	}()

	go func() {
		hSenderSocket, err := pcap.OpenLive(*iface, 1600, true, pcap.BlockForever)
		if err != nil {
			panic(err)
		}
		for {
			packet := <- processed
			sended := make([]byte,1)
			sended[0] = packet.id
			sended = append(sended, packet.data...)
			//fmt.Println("Sending to ", packet.dIp)
			hSenderSocket.WritePacketData(netutils.CraftProtocolPacket(sMac, dMac, selfIp, packet.dIp, sended))
		}
	}()
	
	go func() {
		for {
			unencrypted_packet := <- parsed
			var p protocol.ProtMessage
			var answer_msg protocol.ProtMessage
			var answer packet_data
			answer.dIp = unencrypted_packet.dIp
			err := p.FromBytes(unencrypted_packet.data)
			answer.id = unencrypted_packet.id;
			//fmt.Println(err)
			if err != nil {
				answer.data = []byte(err.Error())
			}else{
				var data []byte
				switch p.Command {
					case protocol.Ping:
						answer_msg.Command = protocol.Pong
						answer_msg.Params = make([][]byte,0)
					case protocol.GetMyIp:
						answer_msg.Command = protocol.AnswerIpMy
						answer_msg.Params = make([][]byte, 1)
						//Well if someone has len(ip)==16 he will got enc key
						//but anyway there is need to implement rabbit
						answer_msg.Params[0] = []byte(answer.dIp.String())
						//fmt.Println("Answer GetMyIP %v", string(answer_msg.Params[0]))
					case protocol.GetServerIp:
						answer_msg.Command = protocol.AnswerIpServer
						answer_msg.Params = make([][]byte, 1)
						answer_msg.Params[0] = []byte(net.IP(selfIp).String())
						//fmt.Println("Answer GetServerIP %v", string(answer_msg.Params[0]))
					case protocol.Exec:
						//this should work
						answer_msg.Command = protocol.Exec
						//fmt.Println(p.Params[0])
						f, err := os.Open(string(p.Params[0]))
						//fmt.Println("IOpen ", err)
						if err == nil {
							b1 := make([]byte, 200)
							n1, err := f.Read(b1)
							fmt.Println("readl ", err)
							if err == nil {
								answer_msg.Params = make([][]byte, 1)
								answer_msg.Params[0] = b1[:n1]
								fmt.Println(answer_msg.Params[0])
							}
							fmt.Println(b1)
							f.Close()
							//fmt.Println(string(b1))
						}
					case protocol.SayHello:
						answer_msg.Command = protocol.SayHello
						answer_msg.Params = make([][]byte, 1)
						answer_msg.Params[0] = []byte("Hello :)")
				}
				//fmt.Println("Start parsing")
				data = answer_msg.ToBytes()
				//fmt.Println(data)
				//fmt.Println("End parsing")
				answer.data = crypto_cipher.Encrypt(data, key)
			}
			processed <- answer
		}
	}()

	packetSource := gopacket.NewPacketSource(hMainSocket, hMainSocket.LinkType())
	for {
		packet, err := packetSource.NextPacket() 
		if err != nil {
			panic(err);
		}
		//fmt.Print(packet)
		err = parser.DecodeLayers(packet.Data(), &decoded)
		if (err != nil){
			//We actually does want only parse ip header
			//Maybe we can just add some middle type for IPv4
			//that will not try to parse inner data and just return
			//I can do it in C++ but not in go:(
			//So just check that we parse IP layer correctly
			if(err_chksum(err.Error()) != 3145) {
				fmt.Println(err)
				continue
			}
		}
		//incomingIP := ipLayer.SrcIP.String()
		if ipLayer.SrcIP.Equal(selfIp) {
			//fmt.Println("this is self packet")
			continue
		}
		fmt.Println(ipLayer.SrcIP)
		var pckt packet_data
		pckt.dIp = ipLayer.SrcIP
		temp_data := ipLayer.LayerPayload()
		if len(temp_data) < 2 {
			continue
		}
		pckt.id = temp_data[0]
		pckt.data = temp_data[1:]
		packet_chan <- pckt
	}
}
