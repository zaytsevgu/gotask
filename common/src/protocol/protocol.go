package protocol

import "fmt"

type ProtMessage struct {
	Length byte
	Command Cmd
	Params [][]byte
}

type Cmd int

const (
	Ping Cmd = iota
	Pong 
	Exec
	SayHello
	GetMyIp
	GetServerIp
	AnswerIpMy
	AnswerIpServer
)

type LengthError struct {
}

func (e *LengthError) Error() (string) {
	return "Invalid length"
}

type InvalidCommand struct {
}

func (e* InvalidCommand) Error() (string) {
	return "Invalid command"
}

type NoCommand struct{
}

func (e *NoCommand) Error() (string){
	return "No command provided"
}

type InvalidParamsCount struct {
	expected     int
	params_count int
}

func (e *InvalidParamsCount) Error() (string) {
	return fmt.Sprintf("Got %v params - expected %v", e.params_count, e.expected)
}

type ErrParamParsing struct {
}

func (e *ErrParamParsing) Error() (string) {
	return "Error while parsing command params"
}

type ExternalData struct {
}

func (e *ExternalData) Error() (string){
	return "All params parsed but some data left"
}
func (p *ProtMessage) FromBytes(packet []byte)(error) {
	if len(packet) == 0 {
		return &LengthError{}
	}
	length := packet[0]
	if int(length) != len(packet) -1 {
		return &LengthError{}
	}
	if length == 0 {
		return &NoCommand{}
	}
	p.Length = length
	switch Cmd(packet[1]) {
		case Ping:
			p.Command = Ping
			if length == 1{
				return nil
			}
			var err error
			p.Params, err = parseParams(packet[2:])
//			fmt.Println(err)
			if err != nil {
				return err
			}
			if len(p.Params) != 0 {
				return &InvalidParamsCount{0, len(p.Params)}
			}
			return nil
		case Exec:
			p.Command = Exec
			if length == 1{
				return &InvalidParamsCount{1, 0}
			}
			var err error
			p.Params, err = parseParams(packet[2:])
			if err != nil {
				return err
			}
			if len(p.Params) != 1 {
				return &InvalidParamsCount{1, len(p.Params)}
			}
			return nil
		case SayHello:
			p.Command = SayHello
			if length == 1{
				return nil
			}
			var err error
			p.Params, err = parseParams(packet[2:])
			if err != nil {
				return err
			}
			if len(p.Params) != 0 {
				return &InvalidParamsCount{0, len(p.Params)}
			}
			return nil
		case GetMyIp:
			p.Command = GetMyIp
			if length == 1{
				return nil
			}
			var err error
			p.Params, err = parseParams(packet[2:])
			if err != nil {
				return err
			}
			if len(p.Params) != 0 {
				return &InvalidParamsCount{0, len(p.Params)}
			}
			return nil
		case GetServerIp:
			p.Command = GetServerIp
			if length == 1{
				return nil
			}
			var err error
			p.Params, err = parseParams(packet[2:])
			if err != nil {
				return err
			}
			if len(p.Params) != 0 {
				return &InvalidParamsCount{0, len(p.Params)}
			}
			return nil
		default:
			return &InvalidCommand{}
	}
	return nil
}

func parseParams(data []byte)([][]byte, error){
	length := data[0]
	if int(length) != len(data)-1 {
		return nil, &LengthError{}
	}
	if length == 0 {
		ret := make([][]byte, 0)
		return ret, nil
	}
	p_count := int(data[1])
	if (p_count == 0) && (length != 1) {
		return nil, &ExternalData{};
	}
	ret := make([][]byte,p_count)
//	fmt.Println("Got ",p_count, "and ", len(data))
	pos := 2
	if (p_count != 0) && length < 2 {
		return nil, &ErrParamParsing{}
	}
	for i:=0;i<p_count; i++{
		var bytes int
		var err error
//		fmt.Println("Got pos ", pos, " ",i)
		if pos > int(length) {
			return nil, &ErrParamParsing{}
		}
		ret[i], bytes, err = parseField(data[pos:])
		if err != nil{
			return nil, &ErrParamParsing{}
		}
		pos += bytes
	}
	return ret, nil
}

func parseField(data []byte)([]byte, int, error){
	ret := make([]byte, 0)
	length := int(data[0])
	if length > len(data)-1 {
		return nil, 0, &ErrParamParsing{}
	}
	ret = make([] byte, 0)
	for i:=1; i<=length; i++ {
		ret = append(ret, data[i])
	}
	return ret, length+1, nil
}

func (p *ProtMessage) ToBytes()([]byte){
	ret := make([]byte, 2)
	ret[0] = 0
	ret[1] = byte(p.Command)
	if (len(p.Params)!=0){
		params := p.paramsToByte()
		ret = append(ret, params...)
	}
	ret[0] = byte(len(ret)-1)
	return ret
}

func (p *ProtMessage)paramsToByte()([]byte){
	ret := make([]byte, 2)
	ret[0] = 0
	ret[1] = byte(len(p.Params))
	for i := 0; i<len(p.Params); i++ {
		ret = append(ret, byte(len(p.Params[i])))
		ret = append(ret, p.Params[i]...)
	}
	ret[0] = byte(len(ret)-1)
	return ret
}
