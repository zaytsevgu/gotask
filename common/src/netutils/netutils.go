package netutils

import (
	"net"
	"fmt"
	"github.com/google/gopacket/routing"
	"github.com/google/gopacket/pcap"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket"
)

var r routing.Router

func init(){
	var err error
	r, err = routing.New()
	if err != nil{
		panic(err)
	}
}

func GetGatewayAndSrc(addr net.IP)(gateway, srcaddr net.IP){
	//fmt.Println(addr)
	//fmt.Println(r)
	_, gateway, addr, err := r.Route(addr)
	if err != nil {
		panic(err)
	}
	return gateway, addr
}

func GetGatewayMACAndSelfIP(ifname string, sMac []byte,  addr net.IP)(gwMAC []byte, sIp []byte){
	gtw, src := GetGatewayAndSrc(addr)
	//fmt.Println(gtw)
	//fmt.Println(src)
	dMac := GetMacAddr(ifname, sMac, src, gtw)
	return dMac, src
}

func CraftProtocolPacket(sMac, dMac, sIP, dIP, payload []byte)(packet []byte){
	ethernetLayer := &layers.Ethernet{
		SrcMAC: sMac,
		DstMAC: dMac,
		EthernetType: layers.EthernetTypeIPv4,
	}
	ipLayer := &layers.IPv4{
		Version:    4,
		IHL:        5,
		TOS:        0,
		Length:     20,
		Id:         2,
		Flags:      layers.IPv4DontFragment,
		FragOffset: 0,
		TTL:        255,
		Protocol:   99,
		Checksum:   0,
		SrcIP:      sIP,
		DstIP:      dIP,
	}
	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{
		FixLengths: true,
		ComputeChecksums: true,
	}

	err := gopacket.SerializeLayers(buf, opts, ethernetLayer, ipLayer, gopacket.Payload(payload));
	if err != nil {
		panic(err)
	}
	return buf.Bytes()
}


func makeArpHeader(HWsrc []byte, srcIP, dstIP []byte) (req []byte){
	brcast, _ := net.ParseMAC("ff:ff:ff:ff:ff:ff")
	ethernetLayer := &layers.Ethernet{
		SrcMAC: HWsrc,
		DstMAC: brcast,
		EthernetType: layers.EthernetTypeARP,
	}
	empty, _ := net.ParseMAC("00:00:00:00:00:00")
	//fmt.Println(srcIP)
	arpLayer := &layers.ARP{
		AddrType : layers.LinkTypeEthernet,
		Protocol : layers.EthernetTypeIPv4,
		HwAddressSize : 6,
		ProtAddressSize : 4,
		Operation : layers.ARPRequest,
		SourceHwAddress : HWsrc,
		SourceProtAddress : srcIP,
		DstHwAddress : empty,
		DstProtAddress : dstIP,
	}

	
	buf := gopacket.NewSerializeBuffer()
	opts := gopacket.SerializeOptions{
		FixLengths: true,
		ComputeChecksums: true,
	}

	err := gopacket.SerializeLayers(buf, opts, ethernetLayer, arpLayer);
	if err != nil {
		panic(err)
	}	

	return buf.Bytes()
}

func GetMacAddr(ifacename string, HWsrc []byte, src, dst net.IP)(HWDst []byte){
	hArp, err := pcap.OpenLive(ifacename, 1600, true, pcap.BlockForever)
	if err != nil {
		panic(err)	
	}
	defer hArp.Close()
	err = hArp.SetBPFFilter("arp")
	if err != nil {
		panic(err)
	}
	//fmt.Println("Asking {} mac address", dst)
	var ethLayer layers.Ethernet
	var arpLayer layers.ARP
	parser := gopacket.NewDecodingLayerParser(layers.LayerTypeEthernet, &ethLayer, &arpLayer)
	decoded := make([]gopacket.LayerType, 0, 4)	
	packetSource := gopacket.NewPacketSource(hArp, hArp.LinkType())
	//Make ARP request
	hArp.WritePacketData(makeArpHeader(HWsrc, src, dst))
	for {
		packet, err := packetSource.NextPacket()
		if err != nil {
			panic(err)
		}
		parser.DecodeLayers(packet.Data(), &decoded)
		if len(decoded) < 2 {
			fmt.Println("Not enough layers!")
			continue
		}
		// We want only ARP Reply
		if (arpLayer.Operation != layers.ARPReply){
			continue
		}
		//fmt.Println("Got apr responce {}",arpLayer);
		// In reply fields are swapped
		if (dst.Equal(arpLayer.SourceProtAddress)) {
			return arpLayer.SourceHwAddress;
		}

	}
	return nil
}

