//package main
package crypto_cipher

type c_instance struct {
	x [8]uint32
	c [8]uint32
	carry uint32
}

func rotl(x uint32, rot uint) (uint32){
	return (x << rot) | (x >> (32-rot))
}

func g_func(x uint32)(uint32){
	var a, b, h, l uint32
	a = x & 0xffff
	b = x >> 16
	h = ((((a * a) >> 17) + (a * b)) >> 15) + (b * b)
	l = x*x
	return h ^ l
}

//I REALLY need this? :(
func bool_to_int(x bool)(uint32){
	if x == true{
		return 1
	}
	return 0
}

func next_state(p_instance *c_instance) {
	var g, c_old [8]uint32
	var i uint32

    	for i = 0; i < 8; i++ {
	        c_old[i] = p_instance.c[i];
	}

    	p_instance.c[0] += 0x4d34d34d + p_instance.carry;
    	p_instance.c[1] += 0xd34d34d3 + bool_to_int(p_instance.c[0] < c_old[0]);
    	p_instance.c[2] += 0x34d34d34 + bool_to_int(p_instance.c[1] < c_old[1]);
    	p_instance.c[3] += 0x4d34d34d + bool_to_int(p_instance.c[2] < c_old[2]);
    	p_instance.c[4] += 0xd34d34d3 + bool_to_int(p_instance.c[3] < c_old[3]);
    	p_instance.c[5] += 0x34d34d34 + bool_to_int(p_instance.c[4] < c_old[4]);
    	p_instance.c[6] += 0x4d34d34d + bool_to_int(p_instance.c[5] < c_old[5]);
    	p_instance.c[7] += 0xd34d34d3 + bool_to_int(p_instance.c[6] < c_old[6]);
    	p_instance.carry = bool_to_int(p_instance.c[7] < c_old[7]);

    	for i = 0; i < 8; i++ {
        	g[i] = g_func(p_instance.x[i] + p_instance.c[i]);
	}
    	p_instance.x[0] = g[0] + rotl(g[7], 16) + rotl(g[6], 16);
    	p_instance.x[1] = g[1] + rotl(g[0],  8) + g[7];
    	p_instance.x[2] = g[2] + rotl(g[1], 16) + rotl(g[0], 16);
    	p_instance.x[3] = g[3] + rotl(g[2],  8) + g[1];
    	p_instance.x[4] = g[4] + rotl(g[3], 16) + rotl(g[2], 16);
    	p_instance.x[5] = g[5] + rotl(g[4],  8) + g[3];
    	p_instance.x[6] = g[6] + rotl(g[5], 16) + rotl(g[4], 16);
    	p_instance.x[7] = g[7] + rotl(g[6],  8) + g[5];
}

func bToI(x []byte)(uint32){
	return uint32(x[3]) << 24 | uint32(x[2]) << 16 | uint32(x[1]) << 8 | uint32(x[0])
}

func key_setup(p_instance *c_instance, p_key []byte) {
    	var k0, k1, k2, k3, i uint32

    	/* Generate four subkeys */
    	k0 = bToI(p_key[0:4])
    	k1 = bToI(p_key[4:8])
    	k2 = bToI(p_key[8:12])
    	k3 = bToI(p_key[12:16])

    	/* Generate initial state variables */
    	p_instance.x[0] = k0;
    	p_instance.x[2] = k1;
    	p_instance.x[4] = k2;
    	p_instance.x[6] = k3;
    	p_instance.x[1] = (k3 << 16) | (k2 >> 16);
    	p_instance.x[3] = (k0 << 16) | (k3 >> 16);
    	p_instance.x[5] = (k1 << 16) | (k0 >> 16);
    	p_instance.x[7] = (k2 << 16) | (k1 >> 16);

    	/* Generate initial counter values */
    	p_instance.c[0] = rotl(k2, 16);
    	p_instance.c[2] = rotl(k3, 16);
    	p_instance.c[4] = rotl(k0, 16);
    	p_instance.c[6] = rotl(k1, 16);
    	p_instance.c[1] = (k0 & 0xFFFF0000) | (k1 & 0xFFFF);
    	p_instance.c[3] = (k1 & 0xFFFF0000) | (k2 & 0xFFFF);
    	p_instance.c[5] = (k2 & 0xFFFF0000) | (k3 & 0xFFFF);
	p_instance.c[7] = (k3 & 0xFFFF0000) | (k0 & 0xFFFF);

    	/* Reset carry flag */
	p_instance.carry = 0;

    	/* Iterate the system four times */
	for i = 0; i < 4; i++ {
		next_state(p_instance);
	}
    	/* Modify the counters */
    	for i = 0; i < 8; i++ {
        	p_instance.c[(i + 4) & 0x7] ^= p_instance.x[i];
	}
}

func ITob(x uint32)([]byte){
	r_byte := make([]byte, 4)
	r_byte[0] = byte(x & 0xff)
	r_byte[1] = byte((x >>8) & 0xff)
	r_byte[2] = byte((x >>16) & 0xff)
	r_byte[3] = byte((x >>24) & 0xff)
	return r_byte	
}

func get_gamma(p_instance *c_instance, size int)([]byte){
	gamma := make([]byte, 0)
	var i int
	for i = 0; i < size; i +=16 {
		next_state(p_instance)
		gamma = append(gamma, ITob(p_instance.x[0] ^ (p_instance.x[5]>>16) ^ (p_instance.x[3]<<16))...)
		gamma = append(gamma, ITob(p_instance.x[2] ^ (p_instance.x[7]>>16) ^ (p_instance.x[5]<<16))...)
		gamma = append(gamma, ITob(p_instance.x[4] ^ (p_instance.x[1]>>16) ^ (p_instance.x[7]<<16))...)
		gamma = append(gamma, ITob(p_instance.x[6] ^ (p_instance.x[3]>>16) ^ (p_instance.x[1]<<16))...)
	}
	return gamma
}
/* Encrypt or decrypt a block of data */
func cipher(p_instance *c_instance, p_src []byte)([]byte) {
	var i int
	ret := make([]byte,len(p_src))
	gamma := get_gamma(p_instance, len(p_src))
	for i = 0; i < len(p_src); i ++ {
		ret[i] = p_src[i] ^ gamma[i]
    	}
	return ret
}


func Encrypt(data, key []byte)([]byte){
	var ctx c_instance
	key_setup(&ctx, key)
	return cipher(&ctx, data)		
}

/*
func main(){
	var plaintext2, ciphertext []byte
	var i int
	plaintext := make([]byte, 512)
	key := make([]byte, 16)
	for i=0;i<512;i++{
		plaintext[i] = 0
	}
	key = []byte{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
	ciphertext = Encrypt(plaintext, key)
	fmt.Println(ciphertext)
	plaintext2 = Encrypt(ciphertext, key)
	fmt.Println(plaintext2)
	key = []byte{ 0xc2, 0x1f, 0xcf, 0x38, 0x81, 0xcd, 0x5e, 0xe8, 0x62, 0x8a, 0xcc, 0xb0, 0xa9, 0x89, 0x0d, 0xf8}
	ciphertext = Encrypt(plaintext, key)
	fmt.Println(ciphertext)
	plaintext2 = Encrypt(ciphertext, key)
	fmt.Println(plaintext2)
}

*/
